<?php

namespace Ega\Products;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductsApiController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $products = [];
        foreach (Product::all() as $product) {
            $productData = [
                'id' => $product->id,
                'name' => $product->name,
            ];
            foreach ($product->attributes as $attribute) {
                $productData['attributes'][$attribute->key] = $attribute->value;
            }
            $products[] = $productData;
        }
        return response()->json(['status' => 'success', 'data' => $products]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductsHtml()
    {
        $products = [];
        $attributes = [];
        foreach (Product::all() as $product) {
            $products[$product->id]['name'] = $product->name;
            foreach ($product->attributes as $attribute) {
                if ($attribute->deleted_at) {
                    continue;
                }
                $products[$product->id][$attribute->key] = $attribute->value;
                if (!in_array($attribute->key, $attributes)){
                    $attributes[] = $attribute->key;
                }
            }
        }

        return view('products::products_table', compact('products', 'attributes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProductsHtmlRawSql()
    {
        $products = [];
        $attributes = [];
        $productsTable = DB::select('SELECT * FROM `products`');
        foreach ($productsTable as $productData) {
            $products[$productData->id]['name'] = $productData->name;
        }

        $attributesTable = DB::select('SELECT * FROM `product_attributes` WHERE `deleted_at` IS NULL');
        foreach ($attributesTable as $attributeData) {
            $products[$attributeData->product_id][$attributeData->key] = $attributeData->value;
            if( !in_array($attributeData->key, $attributes)){
                $attributes[] = $attributeData->key;
            }
        }

        return view('products::products_table', compact('products', 'attributes'));
    }
}
