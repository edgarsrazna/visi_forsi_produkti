<?php

Route::get('all_products',
  'ega\products\ProductsApiController@index');

Route::get('all_products_html',
    'ega\products\ProductsApiController@getProductsHtml');

Route::get('all_products_html_raw_sql',
    'ega\products\ProductsApiController@getProductsHtmlRawSql');
