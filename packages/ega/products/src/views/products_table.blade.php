@if(!empty($products))
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        @foreach($attributes as $attribute)
            <th>{{ ucfirst($attribute) }}</th>
        @endforeach
    </tr>
    @foreach($products as $id => $product)
    <tr>
        <td>{{ $id }}</td>
        <td>{{ $product['name'] }}</td>
        @foreach($attributes as $attribute)
            <td>{{ isset($product[$attribute]) ? $product[$attribute] : '-' }}</td>
        @endforeach
    </tr>
    @endforeach
</table>
@endif
