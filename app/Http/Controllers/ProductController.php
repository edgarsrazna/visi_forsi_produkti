<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $product = Product::create($request->all());

        $attributes = $request->input('attributes');
        if (strpos($attributes, ':') !== false) {
            $this->storeAttributes($product, $attributes);
        }

        return redirect()->route('products.index')
            ->with('success', __('Product created successfully.'));
    }

    /**
     * @param Product $product
     * @param string $attributes
     */
    private function storeAttributes(Product $product, $attributes)
    {
        foreach (explode(';', $attributes) as $attribute) {
            $pos = strpos($attribute, ':');
            if ($pos === false) {
                continue;
            }
            Attribute::create([
                'product_id' => $product->id,
                'key' => trim(substr($attribute, 0, $pos)),
                'value' => trim(substr($attribute, $pos+1))
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $product->update($request->all());

        $this->updateAttributes($product, $request->input('attributes'));

        return redirect()->route('products.index')
            ->with('success', __('Product updated successfully'));
    }

    /**
     * @param Product $product
     * @param $attributes
     */
    private function updateAttributes(Product $product, $attributes)
    {
        $oldAttributes = [];
        foreach ($product->attributes as $attribute) {
            $oldAttributes[$attribute->key] = $attribute->value;
        }

        foreach (explode(';', $attributes) as $attribute) {
            $pos = strpos($attribute, ':');
            if ($pos === false) {
                continue;
            }
            $key = trim(substr($attribute, 0, $pos));
            DB::table('product_attributes')->updateOrInsert(
                    ['product_id' => $product->id, 'key' => $key],
                    ['value' => trim(substr($attribute, $pos+1)), 'deleted_at' => null]
            );
            unset($oldAttributes[$key]);
        }

        foreach($oldAttributes as $key=>$value) {
            DB::table('product_attributes')
                ->where('product_id', '=', $product->id)
                ->where('key', '=', $key)
                ->update(['deleted_at' => Carbon::now()]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')
            ->with('success', __('Product deleted successfully'));
    }
}
