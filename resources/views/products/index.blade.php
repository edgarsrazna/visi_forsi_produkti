<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="pt-6 pb-12 px-8 bg-white overflow-hidden shadow-xl sm:rounded-lg">

                @if ($message = Session::get('success'))
                    <div class="bg-blue-100 border-t border-b border-blue-500 text-blue-700 px-4 py-3 mb-6" role="alert">
                        <p class="text-sm">{{ $message }}</p>
                    </div>
                @endif

                <x-jet-button type="button" class="mb-6" onclick="location.href='<?php echo route('products.create'); ?>'">
                    {{ __('Register a new product') }}
                </x-jet-button>

                @if(count($products))
                <table class="table-auto">
                    <tr>
                        <th class="px-4 py-2">{{ __('Id') }}</th>
                        <th class="px-4 py-2">{{ __('Name') }}</th>
                        <th class="px-4 py-2">{{ __('Description') }}</th>
                        <th class="px-4 py-2">{{ __('Created at') }}</th>
                        <th class="px-4 py-2">{{ __('Action') }}</th>
                    </tr>
                    @foreach ($products as $product)
                        <tr>
                            <td class="border px-4 py-2">{{ $product->id }}</td>
                            <td class="border px-4 py-2">{{ $product->name }}</td>
                            <td class="border px-4 py-2">{{ $product->description }}</td>
                            <td class="border px-4 py-2">{{ $product->created_at }}</td>
                            <td class="border px-4 py-2">
                                <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('products.show', $product->id) }}">{{ __('Show') }}</a>
                                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('products.edit', $product->id) }}">{{ __('Edit') }}</a>
                                    @csrf
                                    @method('DELETE')
                                    <button class="underline text-sm text-gray-600 hover:text-gray-900" type="submit" title="{{ __('Delete') }}">{{ __('Delete') }}</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
</x-app-layout>
