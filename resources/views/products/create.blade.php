<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Register a new product:') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="pt-6 pb-12 px-8 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="mb-6">
                    <a class="text-sm text-gray-600 hover:text-gray-900" href="{{ route('products.index') }}">{{ '<- ' . __('Go back') }}</a>
                </div>
                @if ($errors->any())
                    <div class="mb-6 bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative" role="alert">
                        <strong class="font-bold">There were some problems with your input.</strong>
                        <span class="block sm:inline">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </span>
                    </div>
                @endif

                <form action="{{ route('products.store') }}" method="POST" >
                    @csrf

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="name">{{ __('Name') }}*</label>
                        <input type="text" name="name" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                    </div>

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="description">{{ __('Description') }}</label>
                        <textarea class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="description"></textarea>
                    </div>

                    <div class="mb-4">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="attributes">{{ __('Attributes') }}</label>
                        <input type="text" name="attributes" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <p class="mt-3 text-gray-600 text-xs italic">Example: "color:red; taste:sour"</p>
                    </div>

                    <x-jet-button class="mt-6">{{ __('Submit') }}</x-jet-button>

                </form>
            </div>
        </div>
    </div>
</x-app-layout>
