<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Product:') . ' ' . $product->name }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="pt-6 pb-12 px-8 bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="mb-6">
                    <a class="text-sm text-gray-600 hover:text-gray-900" href="{{ route('products.index') }}">{{ '<- ' . __('Go back') }}</a>
                </div>
                <div>
                    <strong>Name:</strong>
                    {{ $product->name }}
                </div>
                <div>
                    <strong>Description:</strong>
                    {{ $product->description }}
                </div>
                @foreach ($product->attributes as $attribute)
                    <?php if ($attribute->deleted_at) continue; ?>
                    <div>
                        <strong>{{ ucfirst($attribute->key) }}:</strong>
                        {{ $attribute->value }}
                    </div>
                @endforeach
                <div>
                    <strong>Date Created:</strong>
                    {{ $product->created_at }}
                </div>
                <form action="{{ route('products.destroy', $product->id) }}" method="POST">
                    <x-jet-button type="button" class="mt-3 mb-0 mr-2" onclick="location.href='<?php echo route('products.edit', $product->id); ?>'">{{ __('Edit') }}</x-jet-button>
                    @csrf
                    @method('DELETE')
                    <x-jet-button class="mt-3 mb-0" >{{ __('Delete') }}</x-jet-button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
